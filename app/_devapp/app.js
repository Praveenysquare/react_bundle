import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

import JitsiMeetComponent from './JitsiVideo';

import './css/app.css';
/** We are importing our index.php my app Vairaible */


/* globals __webpack_public_path__ */
__webpack_public_path__ = `${window.STATIC_URL}/app/assets/bundle/`;


class Myapp extends Component {
  
    render() {


        return (
            <div style={{ width: "100%", height: "100%" }} className="App">
            <header className="App-header">
         
              <JitsiMeetComponent></JitsiMeetComponent>
            </header>
          </div>
        )
    }
}

render(<Myapp/>, document.getElementById('app'));