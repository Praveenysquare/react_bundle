import React, { useState, useEffect } from 'react';
import ProgressComponent from '@material-ui/core/CircularProgress';
import myApp from 'myApp';
import axios from 'axios';



function JitsiMeetComponent() {
    const [loading, setLoading] = useState(true);
    const containerStyle = {
        width: '100%',
        height: '100%',
   
    };
    const jitsiContainerStyle = {
        display: (loading ? 'none' : 'block'),
        width: '100%',
        height: '100%',
    }

    function startConference() {
        const { user: { roomName, password, userName, returnUrl,designation, scApiBaseUrl,consultationId } } = myApp;

let doctorLeft=false
       if(designation==='doctor')
       {
        try {
            const domain = 'jitsi-poc.securra.com';
            const options = {
                roomName: roomName,
                height: 600,
                parentNode: document.getElementById('jitsi-container'),
                interfaceConfigOverwrite: {

                    SHOW_PROMOTIONAL_CLOSE_PAGE: false,
                    TOOLBAR_BUTTONS: [
                        "microphone",
                        "camera",
                        "hangup",
                        "chat",
                        "tileview",
                        
                        
                    ],
                },
                configOverwrite: {
                    disableSimulcast: false,
                    enableClosePage: true,
                    disableDeepLinking: true

                }
            };

            // eslint-disable-next-line no-undef
            const api = new JitsiMeetExternalAPI(domain, options);
            // set new password for channel
           
           
               //room creation and password creation
        api.addEventListener('videoConferenceJoined', () => {
            console.log('Local User Joined');
            setLoading(false);
            api.executeCommand('displayName', userName);
  
  
            api.addEventListener('participantRoleChanged', function (event) {
              if (event.role === "moderator") {
                api.executeCommand('password', password);
              }
            });
            api.addEventListener('videoConferenceLeft', () => {

                
                if(!doctorLeft){
                    doctorLeft = true;
                    console.log('doctorLeft');
                    if(scApiBaseUrl && consultationId){
                        axios.defaults.baseURL = scApiBaseUrl;
         return axios
            .put(`/consultation/${consultationId}/update?status=ended`)
                        .then((res) => {
                          window.top.location.href = returnUrl;
                        }).catch(err => {
                            console.log(err);
                            window.top.location.href = returnUrl;
                        })
                    }else{
                        window.top.location.href = returnUrl;
                    }
                }
                
      
                
            });
          });
  
          //password required for join
          api.on('passwordRequired', function () {
            api.executeCommand('password', password);
  
  
          });
  
          

               
           
        } catch (error) {
            console.error('Failed to load Jitsi API', error);
        }
    
 }

    else{
        try {
            const domain = 'jitsi-poc.securra.com';
            const options = {
                roomName: roomName,
                height: 600,
                parentNode: document.getElementById('jitsi-container'),
                interfaceConfigOverwrite: {

                    SHOW_PROMOTIONAL_CLOSE_PAGE: false,
                    TOOLBAR_BUTTONS: [
                        "microphone",
                        "camera",
                        "hangup",
                        "chat",
                        "tileview",
                      ],
                },
                configOverwrite: {
                    disableSimulcast: false,
                    enableClosePage: true,
                    disableDeepLinking: true

                },
            };

            // eslint-disable-next-line no-undef
            const api = new JitsiMeetExternalAPI(domain, options);
            // set new password for channel
           
           
               //room creation and password creation
        api.addEventListener('videoConferenceJoined', () => {
            console.log('Local User Joined');
            setLoading(false);
            api.executeCommand('displayName', userName);
  
  
            api.addEventListener('participantRoleChanged', function (event) {
              if (event.role === "moderator") {
                api.executeCommand('password', password);
                api.addEventListener('videoConferenceLeft', () => {
  
                    console.log('doctorLeft');
                      window.top.location.href = returnUrl
                    });
              }
            });
            api.addEventListener('participantLeft', () => {
  
            console.log('patientLeft');
              window.top.location.href = returnUrl
            });
  
  
          });
  
          //password required for join
          api.on('passwordRequired', function () {
            api.executeCommand('password', password);
  
  
          });
  
          

               
           
        } catch (error) {
            console.error('Failed to load Jitsi API', error);
        }
    
    }

}

    useEffect(() => {
        // verify the JitsiMeetExternalAPI constructor is added to the global..
        if (window.JitsiMeetExternalAPI) startConference();
        else alert('Jitsi Meet API script not loaded');
    }, []);
  
    return (
       
        //  
        <div
        style={containerStyle}       >

            {loading && <ProgressComponent 
        style={{marginTop: '25%'}}/>}
        
          
            <div
                id="jitsi-container"
                style={jitsiContainerStyle}
            />
        </div>
      
    );
}

export default JitsiMeetComponent;